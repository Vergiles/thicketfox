﻿using System; using System.Collections.Generic; using System.Diagnostics; using Plugin.Settings; using Xamarin.Forms;  namespace GuessnNumber {     public partial class ViewGame : ContentPage     {         public ViewGame()         {             NavigationPage.SetHasNavigationBar(this, false);             InitializeComponent();         }
         protected override void OnDisappearing()         {             base.OnDisappearing();             var viewModel = BindingContext as ViewGameViewModel;             viewModel.s_timer = false;             // Inform the view model that it is disappearing so that it can remove event handlers             // and perform any other clean-up required..             viewModel?.OnDisappearing();         }          protected override void OnAppearing()         {             base.OnAppearing();             var viewModel = BindingContext as ViewGameViewModel;              if(TableNumbers.Children.Count == 0)                 viewModel.CreateCellNumber(TableNumbers);              viewModel.s_timer = true;             viewModel.TimeRoundGame();             // Inform the view model that it is appearing.             viewModel?.OnAppearing();         } 
        protected override bool OnBackButtonPressed()         {             if (true)                 ExitGame();              return true;         }

        #region ExitGame          /// <summary>         /// Exits the game. выход из уровня         /// </summary>
        private async void ExitGame()         {             var answer = await DisplayAlert(                 "Покинуть уровень",
                "Ты передумал собирать мой опетитные яблочки?",
                "Да", "Нет");              if (answer)                 await Navigation.PopAsync();         }

        #endregion     } }  