﻿
using Xamarin.Forms;

namespace GuessnNumber
{
    public partial class Dashboard : ContentPage
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            var viewModel = BindingContext as DashboardViewModel;

            // Inform the view model that it is disappearing so that it can remove event handlers
            // and perform any other clean-up required..
            viewModel?.OnDisappearing();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var viewModel = BindingContext as DashboardViewModel;

            viewModel.CreateLevelWindow(MenuLevelTable);
            // Inform the view model that it is appearing.
            viewModel?.OnAppearing();
        }
    }
}
