﻿namespace GuessnNumber
{
    /// <summary>
    /// Models. Модель объектов
    /// </summary>
    public class Models : ViewModel
    {
        public static int levelGame;

        /// <summary>
        /// Gets or sets the level game. Уровень игры
        /// </summary>
        /// <value>The level game.</value>
        public int LevelGame
        {
            get { return levelGame; }
            set
            {
                if (levelGame != value)
                {
                    levelGame = value;
                    OnPropertyChanged("LevelGame");
                }
            }
        }
    }
}

