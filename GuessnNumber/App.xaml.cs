﻿using System;
using System.Diagnostics;
using Plugin.Settings;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace GuessnNumber
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            int level = CrossSettings.Current.GetValueOrDefault("level", 25);

            foreach (var item in DataBaseDictionary.BaseLevel)
                if (item.Value <= level)
                    DataBaseDictionary.BaseAccess[item.Value] = true;

            MainPage = new NavigationPage(new Dashboard
            {
                BindingContext = new DashboardViewModel()
            });
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
