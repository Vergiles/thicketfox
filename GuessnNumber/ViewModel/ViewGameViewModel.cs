﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;
using Plugin.Settings;
using Xamarin.Forms;

namespace GuessnNumber
{
    public class ViewGameViewModel : Function
    {
        public bool statusTimer = true; // Статус таймера
        private int min; // Минута
        private int sec; // Секунда
        private bool statusWin; // Стасту победы
        private int countLive; // Количество жизней
        private int correctNumber; // Загаданное число
        private Dictionary<int, StackLayout> collectionNumber; // Словрь вида чисел

        // Время игры
        public string TimeRound { get; set; }
        // Жизний
        public string Live { get; set; }
        // Картинка жизний
        public string ImageLive { get; set; }
        // Текст кнопки Проверить/ В меню
        public string TextButton { get; set; }
        // Введенное число
        public string Number { get; set; }

        // Команда действие кнопки
        public ICommand CommandCheckedNumber { get; }

        private new INavigation Navigation;

        public ViewGameViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            ImageLive = "live.png";
            TextButton = "Проверить";
            CommandCheckedNumber = new Command(CheckedCommand);
            countLive = DataBaseDictionary.BaseLive[LevelGame];
            Live = countLive.ToString();
            correctNumber = new Random().Next(0, LevelGame + 1);
            collectionNumber = new Dictionary<int, StackLayout>();
            min = DataBaseDictionary.BaseTimeGame[LevelGame];
            TimeRoundGame();
        }

        /// <summary>
        /// Creates the cell number.
        /// </summary>
        /// <param name="grid">Grid.</param>
        public void CreateCellNumber(Grid grid) 
        {
            int row = 0;
            int col = 0;

            // Проходим по циклу с указанным уровней, для создание таблицы чисел
            for (int i = 0; i <= LevelGame; i++)
            {
                var view = new StackLayout
                {
                    BackgroundColor = Color.FromHex("#A1887F"),
                    Padding = 1
                };

                var lab = new Label
                {
                    Text = i.ToString(),
                    TextColor = Color.Black,
                    FontSize = 14,
                    BackgroundColor = Color.White,
                    HorizontalTextAlignment = TextAlignment.Center
                };

                view.Children.Add(lab);
                collectionNumber.Add(i, view);

                grid.Children.Add(collectionNumber[i], col, row);
                col++;

                if (col == 10)
                {
                    col = 0;
                    row++;
                }
            }
        }

        /// <summary>
        /// Times the round game. - Время раунда игры
        /// </summary>
        public void TimeRoundGame()
        {
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Debug.WriteLine("min: " + min + " sec: " + sec);

                // Если у нас минута и секунда будут равны нулю то игра окончена
                // или же минусуем минуту с продолжением времени 
                if (min == 0 && sec == 0)
                {
                    statusTimer = false;
                    Live = "0";
                    ImageLive = "deathe.png";
                    TextButton = "Выход";
                    statusWin = true;
                    DisplayAlert("Внимание", "Время истекло, вы проиграли", "Ок");
                }
                else if (sec == 0)
                {
                    min--;
                    sec = 59;
                }

                var last_Time = string.Format("{0:00}", min) + ":" +
                    string.Format("{0:00}", sec);

                TimeRound = statusTimer ? last_Time : "00:00";

                sec--;

                OnPropertyChanged("");

                return statusTimer;
            });
        }

        /// <summary>
        /// Falls the live. - Теряем жизнь или проигрываем игру
        /// </summary>
        /// <param name="live">Live.</param>
        /// <param name="message">Message.</param>
        private void FallLive(int live, string message)
        {
            Live = live.ToString();

            if (live == 0)
            {
                ImageLive = "deathe.png";
                TextButton = "Выход";
                statusWin = true;
                statusTimer = false;
                TimeRound = "00:00";
                throw new Exception("Вы проиграли");
            }

            DisplayAlert("Не угадали", message, "Ок");
            OnPropertyChanged("");
        }

        /// <summary>
        /// Checkeds the command.
        /// </summary>
        /// <param name="sender">Sender.</param>
        private void CheckedCommand(object sender)
        {
            if (sender is Entry entry)
                try
                {
                    // Если мы победили то возвращаемся на предыдущую страницу
                    // или же проверяем следующие условия
                    if (statusWin)
                        Navigation.PopAsync();
                    else
                    {
                        // Проверка на пустоту Number
                        if (string.IsNullOrEmpty(Number))
                            throw new Exception("Поле пустое");

                        var number = Convert.ToInt32(Number);

                        // Если загаданное число и введенное число правильно то
                        // помечаем ее зеленым или же если число больше или 
                        // меньше загаданное помечаем их/его красным
                        if (number == correctNumber)
                        {
                            var num = StyleLabel.NumberTable(Number, Color.Green);

                            collectionNumber[number].Children.Clear();
                            collectionNumber[number].Children.Add(num);

                            // Если есть следующий уровень в словаре то указываем
                            // ему доступен true
                            if (DataBaseDictionary.BaseAccess.ContainsKey(LevelGame + 25))
                                DataBaseDictionary.BaseAccess[LevelGame + 25] = true;

                            // Сохраняем в настройка сколько максимально пройденного уровня
                            CrossSettings.Current.AddOrUpdateValue("level", LevelGame + 25);

                            TextButton = "В меню";
                            statusWin = true;
                            statusTimer = false;
                            TimeRound = "00:00";
                            DisplayAlert("Позравляю", "Вы угадали!", "Ок");
                        }
                        else if (number < correctNumber)
                        {
                            // Проходим по циклу с помечанием какие числа будут красные
                            for (int i = 0; i <= number; i++)
                            {
                                var num = StyleLabel.NumberTable(
                                    i.ToString(), 
                                    Color.Red
                                );

                                collectionNumber[i].Children.Clear();
                                collectionNumber[i].Children.Add(num);
                            }

                            countLive--;
                            FallLive(countLive, "Мое число больше: " + number);
                        }
                        else if (number > correctNumber)
                        {
                            // Проходим по циклу с помечанием какие числа будут красные
                            for (int i = number; i <= LevelGame; i++)
                            {
                                var num = StyleLabel.NumberTable(
                                   i.ToString(), 
                                   Color.Red
                                );

                                collectionNumber[i].Children.Clear();
                                collectionNumber[i].Children.Add(num);
                            }

                            countLive--;
                            FallLive(countLive, "Мое число меньше: " + number);
                        }

                        OnPropertyChanged("");
                    }
                }
                catch (Exception ex)
                {
                    DisplayAlert("Внимание", ex.Message, "Ок");
                }
        }

        /// <summary>
        /// Called when page is appearing.
        /// </summary>
        public virtual void OnAppearing()
        {
            // No default implementation. 
        }

        /// <summary>
        /// Called when the view model is disappearing. View Model clean-up 
        /// should be performed here.
        /// </summary>
        public virtual void OnDisappearing()
        {
            // No default implementation. 
        }
    }
}

