﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace GuessnNumber
{
    public class DashboardViewModel : Function
    {
        public DashboardViewModel()
        {
        }

        /// <summary>
        /// Creates the level window.
        /// </summary>
        /// <param name="grid">Grid.</param>
        public void CreateLevelWindow(Grid grid)
        {
            grid.Children.Clear();

            var Level = DataBaseDictionary.BaseLevel;

            int count = 0;
            int row = 0;

            for (int i = 0; i <= Level.Count - 1; i++)
            {
                if (count == 2)
                {
                    count = 0;
                    row++;
                }

                var levelButton = new Button
                {
                    Text = Level[i].ToString() + "\nЯблок",
                    TextColor = Color.White,
                    CornerRadius = 10,
                    HeightRequest = 100,
                    WidthRequest = 100,
                    BorderWidth = 2
                };

                var status = DataBaseDictionary.BaseAccess[Level[i]];

                if (status)
                {
                    levelButton.IsEnabled = status;
                    levelButton.BackgroundColor = Color.FromHex("#A1887F");
                    levelButton.BorderColor = Color.FromHex("#81C784");

                    levelButton.Clicked += (object sender, EventArgs e) => {
                        var button = (Button)sender;

                        string number = string.Concat(button.Text.Where(
                            char.IsDigit));

                        Debug.WriteLine("number : " + number);

                        LevelGame = Convert.ToInt32(number);

                        OnPropertyChanged("");

                        var navigation = Application.Current.MainPage.Navigation;

                        var page = new ViewGame
                        {
                            BindingContext = new ViewGameViewModel(navigation)
                        };

                        navigation.PushAsync(page);
                    };
                }
                else
                {
                    levelButton.IsEnabled = status;
                    levelButton.BackgroundColor = Color.FromHex("#E0E0E0");
                    levelButton.BorderColor = Color.FromHex("#E0E0E0");
                }

                grid.Children.Add(levelButton, count, row);
                count++;
            }
        }

        /// <summary>
        /// Called when page is appearing.
        /// </summary>
        public virtual void OnAppearing()
        {
            // No default implementation. 
        }

        /// <summary>
        /// Called when the view model is disappearing. View Model clean-up 
        /// should be performed here.
        /// </summary>
        public virtual void OnDisappearing()
        {
            // No default implementation. 
        }
    }
}

