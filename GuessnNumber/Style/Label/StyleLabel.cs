﻿using System;
using Xamarin.Forms;

namespace GuessnNumber
{
    /// <summary>
    /// Style label. Стиль элементов Label
    /// </summary>
    public static class StyleLabel
    {
        #region NumberTable
        /// <summary>
        /// Numbers the table. - Стиль элемента Label, таблица от и до чисел
        /// </summary>
        /// <returns>The table.</returns>
        /// <param name="text">Text.</param>
        /// <param name="color">Color.</param>
        public static Label NumberTable(string text, Color color)
        {
            var label = new Label
            {
                Text = text,
                TextColor = Color.White,
                BackgroundColor = color,
                FontSize = 14,
                HorizontalTextAlignment = TextAlignment.Center
            };

            return label;
        }
        #endregion
    }
}

