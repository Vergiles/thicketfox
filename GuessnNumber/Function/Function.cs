﻿using Xamarin.Forms;

namespace GuessnNumber
{
    /// <summary>
    /// Function. Методы которые могут быть использованы в наследований классов
    /// </summary>
    public class Function : Models
    {
        /// <summary>
        /// Displaies the alert. Всплвающее окно уведомление
        /// </summary>
        /// <param name="header">Header.</param>
        /// <param name="message">Message.</param>
        /// <param name="button">Button.</param>
        public void DisplayAlert(string header, string message, string button) =>
            DependencyService.Get<IDisplayAlert>().ShowAlert(header, message, button);
    }
}

