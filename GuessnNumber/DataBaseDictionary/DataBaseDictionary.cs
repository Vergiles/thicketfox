﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace GuessnNumber
{
    /// <summary>
    /// Data base dictionary. Словарь данных с хранением уровней
    /// </summary>
    public static class DataBaseDictionary
    {
        #region Base_Access
        /// <summary>
        /// The base access. - Доступ уровней
        /// </summary>
        public static Dictionary<int, bool> BaseAccess = new Dictionary<int, bool>
        {
            {25, true},
            {50, false},
            {75, false},
            {100, false},
            {125, false},
            {150, false},
            {175, false},
            {200, false},
            {225, false},
            {250, false}
        };
        #endregion

        #region Base_Live
        /// <summary>
        /// The base live. - Словарь жизней
        /// </summary>
        public static readonly Dictionary<int, int> BaseLive = new Dictionary<int, int>
        {
            {25, 4},
            {50, 4},
            {75, 6},
            {100, 6},
            {125, 8},
            {150, 8},
            {175, 10},
            {200, 10},
            {225, 12},
            {250, 12}
        };
        #endregion

        #region Base_Time
        /// <summary>
        /// The base time game. Словарь времени игры
        /// </summary>
        public static readonly Dictionary<int, int> BaseTimeGame = new Dictionary<int, int>
        {
            {25, 1},
            {50, 1},
            {75, 1},
            {100, 2},
            {125, 2},
            {150, 2},
            {175, 3},
            {200, 3},
            {225, 3},
            {250, 4}
        };
        #endregion

        #region Base_Level
        /// <summary>
        /// The base level. Словарь уровней
        /// </summary>
        public static readonly Dictionary<int, int> BaseLevel = new Dictionary<int, int>
        {
            {0, 25},
            {1, 50},
            {2, 75},
            {3, 100},
            {4, 125},
            {5, 150},
            {6, 175},
            {7, 200},
            {8, 225},
            {9, 250}
        };
        #endregion
    }
}

